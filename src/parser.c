#include "parser.h"

static void large_arg_by_key_cmp(void *a, void *b)
{
    large_arg_by_key *av, *bv;
    av = a;
    bv = b;
    return strcmp(a->key, b->key);
}

void large_parser_init(large_parser_t *parser, large_err_t *err)
{
    parser->argv = NULL;
    parser->argc = 0;

    vec_init(&(parser->args),        sizeof(large_arg_t));
    vec_init(&(parser->args_by_key), sizeof(large_arg_by_key));
}

int large_arg_init(large_arg_t *arg, large_err_t *err)
{
    memset(arg, 0, sizeof(large_arg_t));

    large_vec_init(&(arg->params), sizeof(large_vec_t));

    return 0;
}

void large_arg_kill(large_arg_t *arg)
{
    VEC_FOREACH(&(arg->keys), char *, key)
    {
        free(key);
    }

    vec_kill(&(arg->keys));

    VEC_FOREACH(&(arg->params), large_vec_t *, rep)
    {
        VEC_FOREACH(rep, large_param_t *, param)
        {
            free(param->size);
        }
        vec_kill(rep);
    }

    vec_kill(&(arg->params));
}

void large_parser_add_arg(large_parser_t *parser, large_spec_t *spec, large_err_t *err)
{
    size_t argind;
    large_arg_t *arg;

    arg = large_vec_app(&(parser->args), NULL);

    if (!arg)
    {
        LARGE_ERR(
            err, LARGE_ERR_NOMEM,
            "Could not allocate space for parser argument."
        );
        return -1;
    }

    large_arg_init(arg);

    if (large_arg_from_spec(arg, spec, err) == -1)
    {
        return -1;
    }

    argind = large_vec_len(&(parser->args)) - 1;
    LARGE_VEC_FOREACH(&(arg->keys), const char *, k)
    {
        size_t ind;
        large_arg_by_key_t argbykey;
        argbykey.n   = argind;
        argbykey.key = k;

        ind = vec_bst(&(arg->args_by_key), &argbykey, large_arg_by_key_cmp);
        if (!vec_ins(&(arg->args_by_key), ind, 1, &argbykey))
        {
            LARGE_ERR(
                err, LARGE_ERR_NOMEM,
                "Could not allocate space for arg_by_key."
            );
            return -1;
        }
    }

    if (vec_len(&(arg->keys)) == 0)
    {
        vec_app(&(arg->positional), argind);
    }

    return 0;
} 

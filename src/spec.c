
int large_str_to_narg_spec(large_narg_t *spec, const char *s)
{
    size_t min, max;
    const char *colon;

    if (s == NULL)
    {
        spec->min = 1;
        spec->max = 1;
    }
    else if (strcmp(s, "?") == 0)
    {
        spec->min = 0;
        spec->max = 1;
    }
    else if (strcmp(s, "+") == 0)
    {
        spec->min = 1;
        spec->max = SIZE_MAX;
    }
    else if (strcmp(s, "*") == 0)
    {
        spec->min = 0;
        spec->max = SIZE_MAX;
    }
    else if (s[0] == '>')
    {
        char *end;
        spec->min = strtol(s + 1, &end, 10) + 1;
        spec->max = SIZE_MAX;

        if (*end) return -1;
    }
    else if (s[0] == '<')
    {
        char *end;
        spec->max = strtol(s + 1, &end, 10) - 1;
        spec->min = 0;

        if (*end) return -1;
    }
    else if (s[0] != '\0')
    {
        char *end;
        spec->min = strtol(s, &end, 10);

        if (*end == '\0')
        {
            spec->max = strtol(s, &end, 10);

            if (*end) return -1;
        }
        else if (*end == '-')
        {
            s = end + 1;
            spec->max = strtol(s, &end, 10);

            if (*end) return -1;
        }
        else
        {
            return -1;
        }
    }
    else
    {
        return -1;
    }

    return 0;
}

int large_keys_from_spec(vec_s *keys, const char *s)
{
    enum {
        SPACE,
        DASH,
        KEY,
        PUSH
    } state;
    const char *keystart;
    char *new;

    keystart = NULL;
    state = SPACE;

    while (*s || state == KEY)
    {
        switch (state)
        {
        case SPACE:
            switch (s)
            {
            case ' ':
                ++s;
                break;
            case '-':
                state = DASH;
                break;
            default:
                keystart = s;
                state = KEY;
                break;
            }
            break;
        case DASH:
            switch (s)
            {
            case ' ':
                return -1;
            case '-':
                ++s;
                break;
            default:
                keystart = s;
                state = KEY;
                break;
            }
            break;
        case KEY:
            switch (s)
            {
            case '\0':
            case ' ':
                len = s - keystart;
                new = malloc(1 + len);
                new[len] = '\0';
                memcpy(new, keystart, len);
                vec_app(keys, &new);
                state = SPACE;
                break;
            default:
                ++s;
                break;
            }
        }
    }

    if (vec_len(keys) == 0)
        return -1;

    return 0;
}

int large_arg_from_spec(large_arg_t *arg, large_spec_t *spec, large_err_t *err)
{
    if (large_narg_from_spec(&(arg->nparamspec), spec->nparam) == -1)
    {
        LARGE_ERR(
            err, LARGE_ERR_INVSPEC,
            "Invalid large_spec_t.nparam: '%s'"
        );

        return -1;
    }

    if (large_narg_from_spec(&(arg->nrepspec), spec->nrep) == -1)
    {
        LARGE_ERR(
            err, LARGE_ERR_INVSPEC,
            "Invalid large_spec_t.nrep: '%s'"
        );

        return -1;
    }

    if (large_keys_from_spec(&(arg->keys), spec->keys) == -1)
    {
        LARGE_ERR(
            err, LARGE_ERR_INVSPEC,
            "Invalid large_spec_t.keys: '%s'"
        );

        return -1;
    }

    memcpy(&(arg->act), &(spec->act), sizeof(large_action_t));

    return 0;
}

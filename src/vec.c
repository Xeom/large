#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "util/vec.h"

static int large_vec_resize_longer(large_vec_t *v);
static int large_vec_resize_shorter(large_vec_t *v);

void large_vec_init(large_vec_t *v, size_t width)
{
    memset(v, 0, sizeof(large_vec_t));
    v->width = width;
}

void large_vec_kill(large_vec_t *v)
{
    if (v->mem)
        free(v->mem);

    memset(v, 0, sizeof(large_vec_t));
}

/* Fix vec sizing when increasing length */
static int large_vec_resize_longer(large_vec_t *v)
{
    if (v->used <= v->allocated)
        return 0;

    if (v->allocated == 0)
        v->allocated = 1;

    do
    {
        v->allocated <<= 1;
    } while (v->used > v->allocated);

    v->mem = realloc(v->mem, v->allocated);

    if (!(v->mem)) return -1;

    return 0;
}

/* Fix vec sizing when decreasing length */
static int large_vec_resize_shorter(large_vec_t *v)
{
    if (v->used >= v->allocated >> 2)
        return 0;

    do
    {
        v->allocated >>= 1;
    } while (v->used < v->allocated >> 2);

    v->mem = realloc(v->mem, v->allocated);

    if (!(v->mem)) return -1;

    return 0;
}

void *large_vec_ins(large_vec_t *v, size_t ind, size_t n, const void *mem)
{
    size_t offset, numbytes, bytesafter;

    if (n == 0) return NULL;

    numbytes   = n   * v->width;
    offset     = ind * v->width;
    bytesafter = v->used - offset;

    if (offset > v->used) return NULL;

    v->used += numbytes;
    large_vec_resize_longer(v);

    if (bytesafter != 0)
        memmove(v->mem + offset + numbytes, v->mem + offset, bytesafter);

    if (mem)
        memcpy(v->mem + offset, mem, numbytes);
    else
        memset(v->mem + offset, 0, numbytes);

    return v->mem + offset;
}

int large_vec_del(large_vec_t *v, size_t ind, size_t n)
{
    size_t offset, numbytes, bytesafter;

    if (n == 0) return 0;

    numbytes   = n   * v->width;
    offset     = ind * v->width;
    bytesafter = v->used - offset - numbytes;

    if (offset > v->used || numbytes > v->used - offset) return -1;

    if (bytesafter != 0)
        memmove(v->mem + offset, v->mem + offset + numbytes, bytesafter);

    v->used -= numbytes;
    large_vec_resize_shorter(v);

    return 0;
}

void *large_vec_get(const large_vec_t *v, size_t ind)
{
    size_t offset;

    offset = ind * v->width;

    if (offset >= v->used) return NULL;

    return v->mem + offset;
}

size_t large_vec_len(const large_vec_t *v)
{
    return v->used / v->width;
}

void *large_vec_fmt(large_vec_t *v, const char *fmt, ...)
{
    va_list args;
    void *rtn;
    int n;
    char c;
    size_t ind;


    /* We calculate the required length */
    ind = large_vec_len(v);
    va_start(args, fmt);
    n = vsnprintf(&c, 1, fmt, args);
    va_end(args);

    /* We add the necessary space to the vector *
     * Including space for the final \x00.      */
    large_vec_add(v, n + 1, NULL);

    /* We do the formatting for real */
    va_start(args, fmt);
    vsnprintf(large_vec_get(v, ind), n + 1, fmt, args);
    va_end(args);

    /* We delete the space for the extra \x00 */
    large_vec_del(v, large_vec_len(v) - 1, 1);

    /* We get a pointer to the formatted text */
    rtn = large_vec_get(v, ind);

    return rtn;
}

void large_vec_rev(large_vec_t *v)
{
    long ind1, ind2;

    for (ind1 = 0, ind2 = large_vec_len(v) - 1; ind2 > ind1; ++ind1, --ind2)
    {
        char tmp[v->width];
        memcpy(tmp,              large_vec_get(v, ind1), v->width);
        memcpy(large_vec_get(v, ind1), large_vec_get(v, ind2), v->width);
        memcpy(large_vec_get(v, ind2), tmp,              v->width);
    }
}

/* BISECTION */
size_t large_vec_bst(
    const large_vec_t *v,
    const void *item,
    int (*cmpfunc)(const void *a, const void *b)
)
{
    size_t ltind, gtind;
    int cmp;

    if (!v || !cmpfunc) return 0;
    /* An empty vector returns 0 always */
    if (v->used == 0)   return 0;

    /* ltind and gtind will always be less than and greater than item, *
     * respectively. They are set to the ends of the vec here          */
    ltind = 0;
    gtind = large_vec_len(v) - 1;

    /* Check that ltind and gtind are less than and greater than item, *
     * otherwise return a limit.                                       */
    if (cmpfunc(large_vec_get(v, ltind), item) > 0) return 0;
    if (cmpfunc(large_vec_get(v, gtind), item) < 0) return large_vec_len(v);

    /* We're done when we've narrowed ltind and gtind down to one apart. *
     * Our new bisection index is between them.                          */
    while (ltind + 1 < gtind)
    {
        size_t midind;

        midind = (gtind + ltind) / 2;
        cmp = cmpfunc(large_vec_get(v, midind), item);

        if (cmp  > 0) gtind = midind;
        if (cmp  < 0) ltind = midind;
        if (cmp == 0) return  midind;
    }

    cmp = cmpfunc(large_vec_get(v, ltind), item);

    if (cmp == 0) return ltind;
    else          return gtind;
}

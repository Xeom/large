#if !defined(_LARGE_SPEC_H)
# define _LARGE_SPEC_H
# include "large.h"

typedef struct large_narg_spec large_narg_spec_t;
struct large_narg_spec
{
    size_t min, max;
};

int large_str_to_narg_spec(large_narg_spec_t *spec, const char *s);




#endif

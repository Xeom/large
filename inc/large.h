#if !defined(_LARGE_H)
# define _LARGE_H

typedef struct large_spec large_spec_t;
typedef struct large_arg  large_arg_t;

typedef int (*large_conv_t)(const char *, void **ptr, size_t *size);
typedef int (*large_cb_t)  (large_arg_t *arg);

large_cb_t large_store_true;
large_cb_t large_store_false;
large_cb_t large_store;
large_cb_t large_store_const;
large_cb_t large_incr;
large_cb_t large_store_arr;
large_cb_t large_store_2d_arr;

int large_conv_int(const char *arg, void *ptr);
int large_conv_float(const char *arg, void *ptr);

large_arg_t *large_add_arg(large_parser_t *parser, large_spec_t *spec);

struct large_action
{
    large_conv_t conv; /**< Conversion function. */
    large_cb_t   cb;   /**< Callback function.   */

    void *ptr;

    void *cnst;
    size_t cnstsize;
}

struct large_spec
{
    const char *key;
    const char *name; /**< The internal name of the argument. */
    const char *help; /**< Help info. First line is a summary. */

    const char *nparam; /**< Specification of the number of parameters. */
    const char *nrep;   /**< Specification of the number of reps. */

    large_action_t act;
};

#define LARGE_ERR_LEN 256

enum
{
    LARGE_OK,
    LARGE_ERR_INVSPEC
}

struct large_err
{
    int  type;
    char msg[LARGE_ERR_LEN];
}



#endif

#if !defined(_LARGE_PARSER_H)
# define _LARGE_PARSER_H
# include "vec.h"
# include "large.h"

struct large_arg
{
    large_vec_t keys;

    large_narg_t nparamspec;
    large_narg_t nrepspec;

    large_vec_t paramss;

    large_action_t act;
};

struct large_arg_by_key
{
    const char *key;
    size_t      n;
};

struct large_parser
{
    const char **argv;
    int          argc;

    large_vec_t args;
    large_vec_t args_by_key;
    large_vec_t positional;
};

struct large_param
{
    const char *mem;
    size_t      size;
}

#endif
